package mx.tec.jan21;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class PerritoActivity extends AppCompatActivity {

    private TextView textito;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perrito);

        textito = findViewById(R.id.textView2);

        Intent intent = getIntent();
        String message = intent.getStringExtra("userName") + " age: " + intent.getIntExtra("age", 0);
        textito.setText(message);

    }

    public void finishActivity(View v){

        // go back sending info
        // use an intent!
        Intent intent = new Intent();

        intent.putExtra("returnValue", "going back!");
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
