package mx.tec.jan21;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // variables to keep reference of widgets
    private TextView textito;
    private EditText input;
    private Button b1, b2;
    private static final int PERRITO_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // retrieving references to widgets
        textito = findViewById(R.id.textView);
        input = findViewById(R.id.editText);
        b1 = findViewById(R.id.button);
        b2 = findViewById(R.id.button2);

        textito.setText("THIS IS FROM CODE!");
        input.setHint("HEY THIS IS AN EXCELLENT HINT!");

        b2.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                Toast.makeText(MainActivity.this, "the other button!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // listen for a button press
    // 2 main choices
    // - link through XML
    // - add listener through code

    // LINK THROUGH XML EXAMPLE
    // method signature with return void and receives a View
    public void buttonTest(View v){

        // how to log
        Log.i("Main",  "This is just some information");
        Log.e("Main", "FATAL ERROR THAT IS TERRIBLE.");

        // do the toast!
        // factory method from the Toast class
        Toast.makeText(this, input.getText(), Toast.LENGTH_SHORT).show();

        // intent - a request form to open an activity
        // we don't order an activity to be open, we request

        Intent intent = new Intent(this, PerritoActivity.class);

        intent.putExtra("userName", input.getText().toString());
        intent.putExtra("age", 34);

        //startActivity(intent);
        startActivityForResult(intent, PERRITO_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        if(requestCode == PERRITO_CODE) {

            if(resultCode == Activity.RESULT_OK){

                Toast.makeText(this, data.getStringExtra("returnValue"), Toast.LENGTH_SHORT).show();
            }
        }


    }
}
